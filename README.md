# C++ 学习成长

* [配置环境](docs/config_env.md)
* [基本语法](docs/basic_grammer.md)
* [函数库](docs/libs.md)
* [小例子](docs/cases.md)
* [项目](docs/projects.md)
* [FAQ](docs/FAQ.md)

网站学习资料

1. [菜鸟教程-C++教程](https://www.runoob.com/cplusplus/cpp-tutorial.html)
2. [C语言中文网 C++入门教程](http://c.biancheng.net/cplus/)
3. [gitee仓库 C++的那些事](https://gitee.com/wangdongping/c_cpp_learning/tree/master/CPP/basic)

视频学习资料

1. [B站30个小时快速精通C++](https://www.bilibili.com/video/BV1Lo4y1o717?from=search&seid=9920468028276864875)
2. [求知讲堂2021C语言/C++视频99天完整版](https://www.bilibili.com/video/BV1fZ4y1F7Vt?p=43)


在线工具

1. [菜鸟教程c++在线编译](https://www.runoob.com/try/runcode.php?filename=helloworld&type=cpp)