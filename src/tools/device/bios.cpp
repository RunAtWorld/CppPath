#include <iostream>
#include <cstring>
using namespace std;

void test();
string get_bois_id();

int main(int argc, char *argv[])
{
    cout << "bios id:" << get_bois_id();
}

// 描述:execmd函数执行命令，并将结果存储到result字符串数组中
// 参数:cmd表示要执行的命令
// result是执行的结果存储的字符串数组
// 函数执行成功返回1，失败返回0
int execmd(char *cmd, char *result)
{
    char buffer[128];              //定义缓冲区
    FILE *pipe = _popen(cmd, "r"); //打开管道，并执行命令
    if (!pipe)
        return 0; //返回0表示运行失败

    while (!feof(pipe))
    {
        if (fgets(buffer, 128, pipe))
        { //将管道输出到result中
            strcat(result, buffer);
        }
    }
    _pclose(pipe); //关闭管道
    return 1;      //返回1表示运行成功
}

string get_bois_id()
{
    char result[1024 * 4] = ""; //定义存放结果的字符串数组
    if (1 == execmd("wmic csproduct get uuid", result))
    {
        // cout << result << endl;
        string res = result;
        return res.substr(4);
    }
    // system("pause"); //暂停以查看结果
    return "wrong";
}

void test()
{
    int a = 5;
    for (int i = 0; i < 100; i++)
    {
        a += i;
    }
    int c = a;
    printf("%d", c);
}
