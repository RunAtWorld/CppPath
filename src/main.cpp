#include <iostream>
#include <ctime>
#include <iomanip>

using namespace std;

int print_time() {
    // 基于当前系统的当前日期/时间
    time_t now = time(0);

    // 把 now 转换为字符串形式
    char *dt = ctime(&now);

    cout << "本地日期和时间：" << dt << endl;

    // 把 now 转换为 tm 结构
    tm *gmtm = gmtime(&now);
    dt = asctime(gmtm);
    cout << "UTC 日期和时间：" << dt << endl;
}

int print_date() {
    // 基于当前系统的当前日期/时间
    time_t now = time(0);

    cout << "1970 到目前经过秒数:" << now << endl;

    tm *ltm = localtime(&now);

    // 输出 tm 结构的各个组成部分
    cout << "年: " << 1900 + ltm->tm_year << endl;
    cout << "月: " << 1 + ltm->tm_mon << endl;
    cout << "日: " << ltm->tm_mday << endl;
    cout << "时间: " << ltm->tm_hour << ":";
    cout << ltm->tm_min << ":";
    cout << ltm->tm_sec << endl;
}

int print_cont() {
    cout << setiosflags(ios::left | ios::showpoint);  // 设左对齐，以一般实数方式显示
    cout.precision(5);       // 设置除小数点外有五位有效数字
    cout << 123.456789 << endl;
    cout.width(10);          // 设置显示域宽10
    cout.fill('*');          // 在显示区域空白处用*填充
    cout << resetiosflags(ios::left);  // 清除状态左对齐
    cout << setiosflags(ios::right);   // 设置右对齐
    cout << 123.456789 << endl;
    cout << setiosflags(ios::left | ios::fixed);    // 设左对齐，以固定小数位显示
    cout.precision(3);    // 设置实数显示三位小数
    cout << 999.123456 << endl;
    cout << resetiosflags(ios::left | ios::fixed);  //清除状态左对齐和定点格式
    cout << setiosflags(ios::left | ios::scientific);    //设置左对齐，以科学技术法显示
    cout.precision(3);   //设置保留三位小数
    cout << 123.45678 << endl;
    return 0;
}

int test_cin() {
    int a, b, c;
    cin >> a;
    cin.get();
    cin >> b;
    cin.get();
    cin >> c;
    cout << a << b << c;
}

int main() {
    std::cout << "Hello, World!" << std::endl;
    print_time();
    print_date();
    print_cont();
    test_cin();
    return 0;
}
