#include <iostream>
#include <string.h>
using namespace std;
int test1();

int main()
{
    std::cout << "Hello, World!" << std::endl;
    test1();
    return 0;
}

int test1()
{
    char str[] = "hello world";
    char sec[] = "code world";

    // const char *ptr1 = str;
    char const *ptr1 = str;
    cout << ptr1 << endl;
    strcpy(str, "hi world");
    cout << ptr1 << endl;
    ptr1 = sec; //改变指针指向，ok
    cout << ptr1 << endl;
    sec[0] = 'o'; //使用原来的指针修改字符数组值，ok
    cout << ptr1 << endl;
    //ptr1[0] = 'a';//改变指针指向的值,报错

    char ss[] = "good game";
    char *const ptr2 = ss;
    cout << ptr2 << endl;
    ptr2[0] = 'a'; //改变指针指向的值，ok
    cout << ptr2 << endl;
    strcpy(ptr2, "last");
    cout << ptr2 << endl;
    ss[0] = 'z'; //使用原来的指针修改字符数组值，ok
    cout << ptr2 << endl;
    //ptr2 = sec;//改变指针指向,报错
    return 0;
}
