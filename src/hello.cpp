//
// Created by hudal on 2021/5/7.
//

#include <iostream>

using namespace std;

// main() 是程序开始执行的地方

int main() {
    cout << "Hello World"; // 输出 Hello World
    printf("Hello World\n");
    int a = 10;
    int b = 29;
    int c = a * b;
    cout << "res:" << c << endl;
    cout << "按任意键结束" << endl;
    system("read");
    return 0;
}