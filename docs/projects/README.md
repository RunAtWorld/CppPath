# 项目

## 新人入手项目

基础入门
1. [CPlusPlusThings](https://github.com/Light-City/CPlusPlusThings): C++那些事, [博客](https://light-city.club/sc/)
2. [TheAlgorithms/C-Plus-Plus](https://github.com/TheAlgorithms/C-Plus-Plus): C++ 实现各种算法
3. [CppTemplateTutorial](https://github.com/wuye9036/CppTemplateTutorial): C++ 模板
4. [design-patterns-cpp](https://github.com/JakubVojvoda/design-patterns-cpp): 设计模式

进阶中级

1. [MyTinySTL](https://github.com/Alinshans/MyTinySTL): 自己实现的STL
2. [taylorconor/tinytetris](https://github.com/taylorconor/tinytetris): 终端版俄罗斯方块
3. [microsoft/calculator](https://github.com/microsoft/calculator): 微软计算器的开源实现
4. [TinyWebServer](https://github.com/qinguoyi/TinyWebServer): Linux下C++轻量级Web服务器，串联前面的所有知识。

高阶项目

1. [libhv](https://github.com/ithewei/libhv): 比libevent、libuv更易用的国产网络库
2. [tmux](https://github.com/tmux/tmux)
3. [netdata](https://github.com/netdata/netdata): Monitor everything in real time. For free. [官网](https://www.netdata.cloud/)

## 项目中用到

1. [restclient-cpp](https://github.com/mrtazz/restclient-cpp)

## C++制作16个小游戏

> 参考 [B站C++制作16个小游戏](https://www.bilibili.com/video/BV1ME411K7yg)