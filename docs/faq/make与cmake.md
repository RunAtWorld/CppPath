# make与cmake

写C程序大体步骤为：
1. 用编辑器编写源代码，如.c文件。
2. 用编译器编译代码生成目标文件，如.o。
3. 用链接器连接目标代码生成可执行文件，如.exe。

## make

但如果源文件太多，一个一个编译时就会特别麻烦，于是人们想到，为什么不设计一种类似批处理的程序，来批处理编译源文件呢，于是就有了make工具，它是一个自动化编译工具，你可以使用一条命令实现完全编译。但是你需要编写一个规则文件，make依据它来批处理编译，这个文件就是makefile，所以编写makefile文件也是一个程序员所必备的技能。

## cmake

对于一个大工程，编写makefile实在是件复杂的事，于是人们又想，为什么不设计一个工具，读入所有源文件之后，自动生成makefile呢，于是就出现了cmake工具，它能够输出各种各样的makefile或者project文件,从而帮助程序员减轻负担。但是随之而来也就是编写cmakelist文件，它是cmake所依据的规则。

![](./pics/20180620083108405.png)

具体来说，CMake是一个比make更高级的编译配置工具，它可以根据不同平台、不同的编译器，生成相应的Makefile或者vcproj项目。

通过编写CMakeLists.txt，可以控制生成的Makefile，从而控制编译过程。

CMake自动生成的Makefile不仅可以通过make命令构建项目生成目标文件，还支持安装（make install）、测试安装的程序是否能正确执行（make test，或者ctest）、生成当前平台的安装包（make package）、生成源码包（make package_source）、产生Dashboard显示数据并上传等高级功能，只要在CMakeLists.txt中简单配置，就可以完成很多复杂的功能，包括写测试用例。

如果有嵌套目录，子目录下可以有自己的CMakeLists.txt。

总之，CMake是一个非常强大的编译自动配置工具，支持各种平台，KDE也是用它编译的。

### 使用cmake

（1）安装cmake。

下载地址：http://www.cmake.org/cmake/resources/software.html

根据自己的需要下载相应的包即可，Windows下可以下载zip压缩的绿色版本，还可以下载源代码。


## 有益参考

1. [CMake使用教程](https://www.cnblogs.com/yymn/p/7780816.html)
2. [CMake与Make最简单直接的区别](https://blog.csdn.net/weixin_42491857/article/details/80741060)