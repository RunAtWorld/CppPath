# gcc和g++

gcc: GNU C Compiler

g++: GNU C++ Compiler

## 共同点
均属于the GNU Compiler Collection，gcc是鼻祖，后来才有了g++

g++ == gcc -xc++ -lstdc++ -shared-libgcc

(gcc后面两项都是链接选项，表示g++要相比gcc链接其他库函数)

## 差异点
1. g++会自动链接C++标准库，比如algorith,string,vector等。
2. gcc会根据文件后缀(.c,.cpp)自动识别是C文件还是C++文件，g++均认为是C++文件。
3. gcc编译C文件少很多宏定义，gcc编译C++会多一些宏定义。

如下：

```
#define __GXX_WEAK__ 1
#define __cplusplus 1
#define __DEPRECATED 1
#define __GNUG__ 4
#define __EXCEPTIONS 1
#define __private_extern__ extern
```

## 有益参考

1. [gcc和g++的区别总结](https://blog.csdn.net/zhoutaoccu/article/details/86661281)