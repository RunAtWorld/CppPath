# 基本语法

## 学习

1. 基本语法参考 [菜鸟教程-C++教程](https://www.runoob.com/cplusplus/cpp-tutorial.html)

## 注释

1. 块注释符（/*...*/）是不可以嵌套使用的。
2. **//** - 一般用于单行注释。
3. **#if 0 ... #endif** 属于条件编译，0 即为参数。

