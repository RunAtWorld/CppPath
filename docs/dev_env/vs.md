# Visual Studio 的使用

## debug调试

C++ debug调试 ：The breakpoint will not currently be hit. No symbols have been loaded for this document.

### 1. 编译

以Test为例，右键Properties，选择Configuration Properties下General，设置output directory为exe输出的位置（如D:\test\debug），intermediate directory为中间文件输出位置（如D:\test\debug）。填写要生成的可执行文件的名称和后缀到Target Name和Target Extension，本文为Test.exe。
![properties-general图](pics/1248f88e9273577f5e775c0743e27444.png)

### 2. debug

右键Properties，选择Configuration Properties下Debugging。
![在这里插入图片描述](pics/18c38d55a18ce5db62214f5da341d1f1.png)
设置要debug的command，如D:\test\debug\Test.exe。命令选项（可选，如–test)。命令执行的位置，如D:\test\debug\。
点击Debug-Start Debugging即可进行调试。

### 3. 问题

无法进入断点：The breakpoint will not currently be hit. No symbols have been loaded for this document.
执行Start without Debugging，程序正常运行。执行Start Debugging程序退出，而不进入断点。c#根据pdb文件进行调试，检查是否生成xxx.pdb文件。如没生成，右键Properties，选择Configuration Properties - Linker - Debugging，将Generate Debug Info 从No改为Generate Debug Info。
![在这里插入图片描述](pics/7a9f763ee818d74c423456b32c09b795.png)