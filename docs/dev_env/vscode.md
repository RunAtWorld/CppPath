## 使用vscode开发C++

## 安装基本组件

1. 安装好 [MinGw](docs/config_env/mingw.md) 和 [Cygwin](docs/config_env/cygwin.md)

2. 安装好 [VSCode](https://code.visualstudio.com/)

3. 安装  C/C++ 开发插件

   ![image-20210511212047295](pics/image-20210511212047295.png)


4. 安装  code runner插件: 安装后可直接运行 C/C++ 代码

   ![image-20210919162428184](pics/image-20210919162428184.png)


## 配置调试环境

#### (1).配置编译器

接下来配置编译器路径，按快捷键Ctrl+Shift+P调出命令面板，输入C/C++，选择“Edit Configurations(UI)”进入配置。这里配置两个选项： 

- 编译器路径：D:\\cygwin\\opt\\windows_64\\bin\\g++.exe

```text
这里的路径根据大家自己安装的Mingw编译器位置和配置的环境变量位置所决定。
```

- IntelliSense 模式：gcc-x64

![image-20210511213618142](pics/image-20210511213618142.png)

![image-20210511213556693](pics/image-20210511213556693.png)

配置完成后，此时在侧边栏可以发现多了一个.vscode文件夹，并且里面有一个c_cpp_properties.json文件，内容如下，说明上述配置成功。现在可以通过 Ctrl+` 快捷键打开内置终端并进行编译运行了。

```json
{
    "configurations": [
        {
            "name": "Win32",
            "includePath": [
                "${workspaceFolder}/**"
            ],
            "defines": [
                "_DEBUG",
                "UNICODE",
                "_UNICODE"
            ],
            //此处是编译器路径，以后可直接在此修改
            "compilerPath": "D:\\cygwin\\opt\\windows_64\\bin\\g++.exe",
            "cStandard": "c11",
            "cppStandard": "c++17",
            "intelliSenseMode": "gcc-x64"
        }
    ],
    "version": 4
}
```

#### (2).配置构建任务

接下来，创建一个tasks.json文件来告诉VS Code如何构建（编译）程序。该任务将调用g++编译器基于源代码创建可执行文件。 

打开一个C++源程序，按快捷键Ctrl+Shift+P调出命令面板，输入tasks，选择“Tasks:Configure Default Build Task”：

![image-20210511213659410](pics/image-20210511213659410.png)

再选择“C/C++: g++.exe build active file”：

![image-20210511213837772](pics/image-20210511213837772.png)

![image-20210511214105185](pics/image-20210511214105185.png)

此时会出现一个名为tasks.json的配置文件，内容如下：

```json
{
    // See https://go.microsoft.com/fwlink/?LinkId=733558 
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
        {
            "type": "shell",
            "label": "g++.exe build active file",//任务的名字，就是刚才在命令面板中选择的时候所看到的，可以自己设置
            "command": "D:/mingw-w64/x86_64-8.1.0-win32-seh-rt_v6-rev0/mingw64/bin/g++.exe",
            "args": [//编译时候的参数
                "-g",//添加gdb调试选项
                "${file}",
                "-o",//指定生成可执行文件的名称
                "${fileDirname}\\${fileBasenameNoExtension}.exe"
            ],
            "options": {
                "cwd": "D:/mingw-w64/x86_64-8.1.0-win32-seh-rt_v6-rev0/mingw64/bin"
            },
            "problemMatcher": [
                "$gcc"
            ],
            "group": {
                "kind": "build",
                "isDefault": true//表示快捷键Ctrl+Shift+B可以运行该任务
            }
        }
    ]
}
```

#### (3).配置调试设置

这里主要是为了在.vscode文件夹中产生一个launch.json文件，用来配置调试的相关信息。

1. 点击菜单栏的*Debug*--Start Debugging

2. 选择C++(GDB/LLDB)
3. 接着会产生一个launch.json的文件：
4. 接下来读者可以点击Add Configuration按钮自己添加配置，也可以直接将笔者配置好的json文件内容复制过去，因为些配置对新手不是特别友好，相关具体细节还是需要参考官方文档。下面是笔者的launch.json文件的内容：

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "g++.exe - 生成和调试活动文件",
            "type": "cppdbg",
            "request": "launch",
            "program": "${fileDirname}\\${fileBasenameNoExtension}.exe",
            "args": [],
            "stopAtEntry": false,
            "cwd": "${workspaceFolder}",
            "environment": [],
            "externalConsole": false,
            "MIMode": "gdb",
            "miDebuggerPath": "D:\\cygwin\\opt\\windows_64\\bin\\gdb.exe",
            "setupCommands": [
                {
                    "description": "为 gdb 启用整齐打印",
                    "text": "-enable-pretty-printing",
                    "ignoreFailures": true
                }
            ],
            "preLaunchTask": "C/C++: g++.exe 生成活动文件"
        }
    ]
}
```

现编写一个hello.cpp文件测试调试，设置断点后，按下F5进入调试，如图成功调试， 左侧为变量内容：

![image-20210511214643915](pics/image-20210511214643915.png)

## FAQ
1. 解决VSCODE无法将“g++”项识别为 cmdlet、函数、脚本文件或可运行程序的名称。
   g++ : 无法将“g++”项识别为 cmdlet、函数、脚本文件或可运行程序的名称。请检查名称的拼写，如果包括路径，请确保路径正确，然后再试一次。
   
   解决方法：
    
    右键“此电脑”——“属性”——“高级系统设置”——“环境变量”，在“系统变量”的“Path”中添加变量值“C:\MinGW\bin\”
    
    点击“确认”保存后，重启Vscode即可。

# 有益参考

1. [C++环境设置](https://www.runoob.com/cplusplus/cpp-environment-setup.html)
