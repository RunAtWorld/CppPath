# GDB调试

GNU symbolic debugger，简称「**GDB 调试器**」，是 Linux 平台下最常用的一款程序调试器。GDB 编译器通常以 gdb 命令的形式在终端（Shell）中使用，它有很多选项，这是我们要重点学习的。

发展至今，GDB 调试器已经对 C、C++、Go、Objective-C、OpenCL、Ada 等多种编程语言提供了支持。实际场景中，GDB 更常用来调试 C 和 C++ 程序，虽然 Linux 平台下有很多能编写 C、C++ 代码的集成开发工具（IDE），但它们调试代码的能力往往都源自 GDB 调试器。

调试是开发流程中一个非常重要的环境，每个程序员都应具备调试代码的能力，尤其对于从事 Linux C/C++ 开发的读者，必须具备熟练使用 GDB 调试器的能力。这套 GDB 入门教程通俗易懂，深入浅出，能让你快速学会使用 GDB 编译器。

## GDB是什么

程序中的错误主要分为 2 类，分别为语法错误和逻辑错误：

- 程序中的语法错误几乎都可以由编译器诊断出来，很容易就能发现并解决；
- 逻辑错误指的是代码思路或者设计上的缺陷，程序出现逻辑错误的症状是：代码能够编译通过，没有语法错误，但是运行结果不对。对于这类错误，只能靠我们自己去发现和纠正。


解决逻辑错误最高效的方法，就是借助调试工具对程序进行调试。通过调试程序，我们可以监控程序执行的每一个细节，包括变量的值、函数的调用过程、内存中数据、线程的调度等，从而发现隐藏的错误或者低效的代码。就好像编译程序需要借助专业的编译器，调试程序也需要借助专业的辅助工具，即调试器（Debugger）。下表罗列了当下最流行的几款调试器：

| 调试器名称      | 特 点                                                        |
| --------------- | ------------------------------------------------------------ |
| Remote Debugger | Remote Debugger 是 VC/VS 自带的调试器，与整个IDE无缝衔接，使用非常方便。 |
| WinDbg          | 大名鼎鼎的 Windows 下的调试器，它的功能甚至超越了 Remote Debugger，它还有一个命令行版本（cdb.exe），但是这个命令行版本的调试器指令比较复杂，不建议初学者使用。 |
| LLDB            | XCode 自带的调试器，Mac OS X 下开发必备调试器。              |
| GDB             | Linux 下使用最多的一款调试器，也有 Windows 的移植版。        |

GDB诞生于 GNU 计划（同时诞生的还有 GCC、Emacs 等），是 Linux 下常用的程序调试器。发展至今，GDB 已经迭代了诸多个版本，当下的 GDB 支持调试多种编程语言编写的程序，包括 C、C++、Go、Objective-C、OpenCL、Ada 等。实际场景中，GDB 更常用来调试 C 和 C++ 程序。

借助 GDB 调试器可以实现以下几个功能：

1. 程序启动时，可以按照我们自定义的要求运行程序，例如设置参数和环境变量；
2. 可使被调试程序在指定代码处暂停运行，并查看当前程序的运行状态（例如当前变量的值，函数的执行结果等），即支持断点调试；
3. 程序执行过程中，可以改变某个变量的值，还可以改变代码的执行顺序，从而尝试修改程序中出现的逻辑错误。

正如**从事 Windows C/C++ 开发的一定要熟悉 Visual Studio**、从事 Java 开发的要熟悉 Eclipse 或 IntelliJ IDEA、从事 Android 开发的要熟悉 Android Studio、从事 iOS 开发的要熟悉 XCode 一样，**从事 Linux C/C++ 开发要熟悉 GDB**。 

虽然 Linux 系统下读者编写 C/C++ 代码的 IDE 可以自由选择，但调试生成的 C/C++ 程序一定是直接或者间接使用 GDB。可以毫不夸张地说，我所做那些 C/C++ 项目的开发和调试包括故障排查都是利用 GDB 完成的，调试是开发流程中一个非常重要的环节，因此对于从事 Linux C/C++ 的开发人员熟练使用 GDB 调试是一项基本要求。 


# 安装GDB

基于 Linux 系统的免费、开源，衍生出了多个不同的 Linux 版本，比如 Redhat、CentOS、Ubuntu、Debian 等。这些 Linux 发行版中，有些默认安装有 GDB 调试器，但有些默认不安装。
判断当前 Linux 发行版是否安装有 GDB 的方法也很简单，就是在命令行窗口中执行 gdb -v 命令。以本机安装的 CentOS 系统为例：如上所示，执行结果为“command not found”，表明当前系统中未安装 GDB 调试器。反之，若执行结果为：则表明当前系统安装了 GDB 调试器。

1. 直接调用该操作系统内拥有的 GDB 安装包，使用包管理器进行安装。此安装方式的好处是速度快，但通常情况下安装的并非 GDB 的最新版本；
2. 前往 GDB 官网下载源码包，在本机编译安装。此安装方式的好处是可以任意选择 GDB 的版本，但由于安装过程需要编译源码，因此安装速度较慢。

> 注意，不同的 Linux 发行版，管理包的工具也不同。根据维护团体（商业公司维护和社区组织维护）的不同，可以将众多 Linux 发行版分为 2 个系列，分别为 RedHat 系列和 Debian 系列。其中 RedHat 系列代表 Linux 发行版有 RedHat、CentOS、Fedora 等，使用 yum 作为包管理器；Debian 系列有 Debian、Ubuntu 等，使用 apt 作为包管理器。

## 快速安装GDB

对于 RedHat 系列的 Linux 发行版，通过在命令行窗口中执行`sudo yum -y install gdb`

```
[root@bogon ~]# gdb -v
bash: gdb: command not found                        <--当前系统中没有GDB 
[root@bogon ~]# sudo yum -y install gdb               <--安装 GDB
Loaded plugins: fastestmirror, refresh-packagekit, security
Loading mirror speeds from cached hostfile
......  <-省略部分过程
Installed:
 gdb.x86_64 0:7.2-92.el6                           

Complete!
[root@bogon ~]# gdb -v
GNU gdb (GDB) Red Hat Enterprise Linux (7.2-92.el6)        <--安装成功
```

可以看到，GDB 安装成功。
对于 Debian 系列的 Linux 发行版，通过执行` sudo apt -y install gdb `指令，即可实现 GDB 的安装。

## 源码安装GDB

和使用 yum（apt）自动安装 GDB 不同，手动安装需提前到 GDB 官网下载相应的源码包，读者可直接点击 [GDB源码包](http://ftp.gnu.org/gnu/gdb/)

> 注意，在安装 GDB 之前，读者必须保证当前操作系统中有可以使用的编译器，比如最常用的 GCC 编译器（应同时支持 gcc 和 g++ 指令），有关 GCC 编译器的下载和安装，读者可阅读《[GCC下载和安装](http://c.biancheng.net/view/7933.html)》一文。另外，源码安装 GDB 需要用到 Makefile 相关的知识，读者可完全遵循以下步骤“照猫画虎”地安装 GDB。对 Makefile 感兴趣的读者，可前往《[Makefile教程](http://c.biancheng.net/makefile/)》做系统了解。


本节下载的 GDB 源码包为 gdb-9.2-tar.gz，接下来以 CentOS 系统为例（也同样适用于其它 Linux 发行版），给大家演示整个安装过程：

1) 找到 gdb-9.2-tar.gz 文件，笔者将下载好的 gdb-9.2-tat.gz 放置在 /usr/local/src 目录下：

```
[root@bogon ~]# tar -zxvf gdb-9.2.tar.gz
--省略解压过程的输出结果
[root@bogon src]# ls
gdb-9.2 gdb-9.2.tar.gz
```

2) 使用 tar 命令解压该文件，执行命令如下：

```
[root@bogon ~]# tar -zxvf gdb-9.2.tar.gz
--省略解压过程的输出结果
[root@bogon src]# ls
gdb-9.2 gdb-9.2.tar.gz
```

此步骤会得到 gdb-9.2.tar.gz 相应的解压文件 gdb-9.2 。
3) 进入 gdb-9.2 目录文件，创建一个 gdb_build_9.2 目录并进入，为后续下载并放置安装 GDB 所需的依赖项做准备：

```
[root@bogon src]# cd gdb-9.2
[root@bogon gdb-9.2]# mkdir gdb-build-9.2
[root@bogon src]# cd gdb-build-9.2
```

4) 在此基础上，继续执行如下指令：

```
[root@bogon gdb-build-9.2]# ../configure
......  <--省略众多输出
configure: creating ./config.status
config.status: creating Makefile
```

5) 执行 make 指令编译整个 GDB 源码文件，此过程可能会花费很长时间，读者耐心等待即可：

```
[root@bogon gdb-build-9.2]# make
...... <-- 省略编译过程产生的输出结果
```

> 注意，如果编译过程中出现错误，极有可能是所用的 GCC 编译器版本过低导致的，可尝试升级 GCC 版本后再重新执行 make 命令。

6) 确定整个编译过程没有出错之后，执行` sudo make install ` 指令 （其中使用 sudo 指令是为了避免操作权限不够而导致安装失败），正式开始安装 GDB 调试器：

```
 [root@bogon gdb-build-9.2]# sudo make install
...... <-- 省略输出结果 
```

以上过程全部成功执行，则表示 GDB 安装成功。通过再次执行 gdb -v 指令，可验证其是否被成功安装。

```
[root@bogon gdb-build-9.2]# gdb -v
GNU gdb (GDB) 9.2
Copyright (C) 2020 Free Software Foundation, Inc.
......  <-- 省略部分输出
```
# GDB调试

如下是一段能够正常编译运行的 C 语言程序：

```C
#include <stdio.h>
int main ()
{
    unsigned long long int n, sum;
    n = 1;
    sum = 0;
    while (n <= 100)
    {
        sum = sum + n;
        n = n + 1;
    }
    return 0;
}
```

> 此源码的完整存储路径为 /tmp/demo/main.c。

本节就以此程序为例，给大家演示 GDB 调试器的基本用法。

## 使用GDB的前期准备

通过前面的学习我们知道，GDB 的主要功能就是监控程序的执行流程。这也就意味着，只有当源程序文件编译为可执行文件并执行时，GDB 才会派上用场。
Linux 发行版中，经常使用 GCC 编译 C、C++ 程序（有关 GCC 编译器，读者可猛击 《[GCC编译器](http://c.biancheng.net/gcc/)》 系统学习）。但需要注意的是，仅使用 gcc（或 g++）命令编译生成的可执行文件，是无法借助 GDB 进行调试的。

```
[root@bogon demo]# ls
main.c
[root@bogon demo]# gcc main.c -o main.exe
[root@bogon demo]# ls
main.c main.exe
```

可以看到，这里已经生成了 main.c 对应的执行文件 main.exe，但值得一提的是，此文件不支持使用 GDB 进行调试。原因很简单，使用 GDB 调试某个可执行文件，该文件中必须包含必要的调试信息（比如各行代码所在的行号、包含程序中所有变量名称的列表（又称为符号表）等），而上面生成的 main.exe 则没有。

那么，如何生成符合 GDB 调试要求的可执行文件呢？很简单，只需要使用 gcc -g 选项编译源文件，即可生成满足 GDB 要求的可执行文件。仍以 main.c 源程序文件为例：

```
[root@bogon demo]# ls
main.c
[root@bogon demo]# gcc main.c -o main.exe -g
[root@bogon demo]# ls
main.c  main.exe
```

由此生成的 main.exe，即可使用 GDB 进行调试。

> 较早以前的 C 语言编译器也允许使用 -gg 选项来产生调试信息，但是现在版本的 GDB 不再支持这种格式产生的调试信息，所以不建议使用 -gg 选项。 

值得一提的是，GCC 编译器支持 -O（等于同 -O1，优化生成的目标文件）和 -g 一起参与编译。GCC 编译过程对进行优化的程度可分为 5 个等级，分别为 O0~O4，O0 表示不优化（默认选项），从 O1 ~ O4 优化级别越来越高，O4 最高。

> 所谓优化，例如省略掉代码中从未使用过的变量、直接将常量表达式用结果值代替等等，这些操作会缩减目标文件所包含的代码量，提高最终生成的可执行文件的运行效率。 

而相对于 -O -g 选项，对 GDB 调试器更友好的是 -Og 选项，-Og 对代码所做的优化程序介于 O0 ~ O1 之间，真正可做到“在保持快速编译和良好调试体验的同时，提供较为合理的优化级别”。

 解决了如何生成满足 GDB 调试器要求的可执行文件，接下来正式学习 GDB 调试器的使用。 

## 启动GDB调试器

在生成包含调试信息的 main.exe 可执行文件的基础上，启动 GDB 调试器的指令如下：

```
[root@bogon demo]# gdb main.exe
GNU gdb (GDB) 8.0.1
Copyright (C) 2017 Free Software Foundation, Inc.
......
(gdb) 
```

注意，该指令在启动 GDB 的同时，会打印出一堆免责条款。通过添加 --silent（或者 -q、--quiet）选项，可将比部分信息屏蔽掉：

```
[root@bogon demo]# gdb main.exe --silent
Reading symbols from main.exe...(no debugging symbols found)...done.
(gdb) 
```

无论使用以上哪种方式，最终都可以启动 GDB 调试器，启动成功的标志就是最终输出的 (gdb)。通过在 (gdb) 后面输入指令，即可调用 GDB 调试进行对应的调试工作。
GDB 调试器提供有大量的调试选项，可满足大部分场景中调试代码的需要。如下表 所示，罗列了几个最常用的调试指令及各自的作用：

| 调试指令                    | 作 用                                                        |
| --------------------------- | ------------------------------------------------------------ |
| (gdb) break xxx (gdb) b xxx | 在源代码指定的某一行设置断点，其中 xxx 用于指定具体打断点的位置。 |
| (gdb) run (gdb) r           | 执行被调试的程序，其会自动在第一个断点处暂停执行。           |
| (gdb) continue (gdb) c      | 当程序在某一断点处停止运行后，使用该指令可以继续执行，直至遇到下一个断点或者程序结束。 |
| (gdb) next (gdb) n          | 令程序一行代码一行代码的执行。                               |
| (gdb) print xxx (gdb) p xxx | 打印指定变量的值，其中 xxx 指的就是某一变量名。              |
| (gdb) list (gdb) l          | 显示源程序代码的内容，包括各行代码所在的行号。               |
| (gdb) quit (gdb) q          | 终止调试。                                                   |

> 如上所示，每一个指令既可以使用全拼，也可以使用其首字母表示。另外，表 1 中罗列的指令仅是冰山一角，GDB 还提供有大量的选项，可以通过 help 选项来查看。有关 help 选项的具体用法，读者可阅读《[GDB查看命令](http://c.biancheng.net/view/7418.html)》一节，这里不再做具体赘述。

仍以 main.exe 可执行程序为例，接下来为读者演示表 1 中部分选项的功能和用法：

```
(gdb) l                      <-- 显示带行号的源代码
1 #include <stdio.h>
2 int main ()
3 {
4     unsigned long long int n, sum;
5     n = 1;
6     sum = 0;
7     while (n <= 100)
8     {
9         sum = sum + n;
10         n = n + 1;
(gdb)                      <-- 默认情况下，l 选项只显示 10 行源代码，如果查看后续代码，安装 Enter 回车即可                                                               
11     }
12     return 0;
13 }
(gdb) b 7               <-- 在第 7 行源代码处打断点
Breakpoint 1 at 0x400504: file main.c, line 7.
(gdb) r                   <-- 运行程序，遇到断点停止
Starting program: /home/mozhiyan/demo1/main.exe

Breakpoint 1, main () at main.c:7
7     while (n <= 100)
Missing separate debuginfos, use: debuginfo-install glibc-2.17-55.el7.x86_64
(gdb) p n               <-- 查看代码中变量 n 的值
$1 = 1                   <-- 当前 n 的值为 1，$1 表示该变量所在存储区的名称
(gdb) b 12             <-- 在程序第 12 行处打断点
Breakpoint 2 at 0x40051a: file main.c, line 12.
(gdb) c                  <-- 继续执行程序
Continuing.

Breakpoint 2, main () at main.c:12
12     return 0;
(gdb) p n               <-- 查看当前 n 变量的值
$2 = 101               <-- 当前 n 的值为 101
(gdb) q                  <-- 退出调试
A debugging session is active.

Inferior 1 [process 3080] will be killed.

Quit anyway? (y or n) y                 <-- 确实是否退出调试，y 为退出，n 为不退出
[root@bogon demo]#
```

# GDB的run命令

根据不同场景的需要，GDB 调试器提供了多种方式来启动目标程序，其中最常用的就是 run 指令，其次为 start 指令。也就是说，run 和 start 指令都可以用来在 GDB 调试器中启动程序，它们之间的区别是：

- 默认情况下，run 指令会一直执行程序，直到执行结束。如果程序中手动设置有断点，则 run 指令会执行程序至第一个断点处；
- start 指令会执行程序至 main() 主函数的起始位置，即在 main() 函数的第一行语句处停止执行（该行代码尚未执行）。

> 可以这样理解，使用 start 指令启动程序，完全等价于先在 main() 主函数起始位置设置一个断点，然后再使用 run 指令启动程序。另外，程序执行过程中使用 run 或者 start 指令，表示的是重新启动程序。

问一个问题，GDB 调试器启动后是否就可以直接使用 run 或者 start 指令了呢？答案当然是否定的。我们知道，启动 GDB 调试器的方式有多种，其中简单的方法就是直接使用 gdb 指令，例如：

```
[root@bogon demo]# gdb
GNU gdb (GDB) 8.0.1
Copyright (C) 2017 Free Software Foundation, Inc.
...... <-- 省略部分输出信息
Type "apropos word" to search for commands related to "word".
(gdb)
```

注意，使用此方式启动的 GDB 调试器，尚未指定要调试的目标程序，何谈使用 run 或者 start 指令呢？

不仅如此，在进行 run 或者 start 指令启动目标程序之前，还可能需要做一些必要的准备工作，大致包括以下几个方面：

- 如果启动 GDB 调试器时未指定要调试的目标程序，或者由于各种原因 GDB 调试器并为找到所指定的目标程序，这种情况下就需要再次手动指定；
- 有些 C 或者 C++ 程序的执行，需要接收一些参数（程序中用 argc 和 argv[] 接收）；
- 目标程序在执行过程中，可能需要临时设置 PATH 环境变量；
- 默认情况下，GDB 调试器将启动时所在的目录作为工作目录，但很多情况下，该目录并不符合要求，需要在启动程序手动为 GDB 调试器指定工作目录。
- 默认情况下，GDB 调试器启动程序后，会接收键盘临时输入的数据，并将执行结果会打印在屏幕上。但 GDB 调试器允许对执行程序的输入和输出进行重定向，使其从文件或其它终端接收输入，或者将执行结果输出到文件或其它终端。


假设使用 GDB 调试器调试如下程序：

```C
//其生成的可执行文件为 main.exe，位于同一路径下
#include<stdio.h>
int main(int argc,char* argv[])
{
    FILE * fp;
    if((fp = fopen(argv[1],"r")) == NULL){
        printf("file open fail");
    }
    else{
        printf("file open true");
    }
    return 0;
}
```

 要知道，命令行窗口打开时默认位于 ~ （表示当前用户的主目录）路径下，假设我们就位于此目录中使用 gdb 命令启动 GDB 调试器，则在执行 main.exe 之前，有以下几项操作要做：

1) 首先，对于已启动的 GDB 调试器，我们可以先通过 l （小写的 L）指令验证其是否已找到指定的目标程序文件： 

```
[root@bogon ~]# gdb -q     <-- 使用 -q 选项，可以省略不必要的输出信息
(gdb) l
No symbol table is loaded.  Use the "file" command.
```

 可以看到，对于找不到目标程序文件的 GDB 调试器，l 指令的执行结果显示“无法加载符号表”。这种情况下，我们就必须手动为其指定要调试的目标程序，例如： 

```
(gdb) file /tmp/demo/main.exe
Reading symbols from /tmp/demo/main.exe...
(gdb) l
1 #include<stdio.h>
2 int main(int argc,char* argv[])
3 {
4     FILE * fp;
5     if((fp = fopen(argv[1],"r")) == NULL){
6         printf("file open fail");
7     }
8     else{
9         printf("file open true");
10     }
(gdb)
11     return 0;
12 }
(gdb)
```

可以看到，通过借助 file 命令，则无需重启 GDB 调试器也能指定要调试的目标程序文件。

> 除了 file 指令外，GDB 调试器还提供有其它的指定目标调试文件的指令，感兴趣的读者可千万[ GDB 官网](https://sourceware.org/gdb/current/onlinedocs/gdb/Files.html#Files)做详细了解，后续章节在用到时也会做详细的讲解。


2) 通过分析 main.c 中程序的逻辑不难发现，要想其正确执行，必须在执行程序的同时给它传递一个目标文件的文件名。

 总的来说，为 GDB 调试器指定的目标程序传递参数，常用的方法有 3 种：
1、启动 GDB 调试器时，可以在指定目标调试程序的同时，使用 --args 选项指定需要传递给该程序的数据。仍以 main.exe 程序为例： 

```
[root@bogon demo]# gdb --args main.exe a.txt
```

整个指令的意思是：启动 GDB 调试器调试 main.exe 程序，并为其传递 "a.txt" 这个字符串（其会被 argv[] 字符串数组接收）。

2、GDB 调试器启动后，可以借助 set args 命令指定目标调试程序启动所需要的数据。仍以 main.exe 为例：

```
(gdb) set args a.txt
```

该命令表示将 "a.txt" 传递给将要调试的目标程序。
3、除此之外，还可以使用 run 或者 start 启动目标程序时，指定其所需要的数据。例如：以上 2 条命令都可以将 "a.txt" 传递给要调试的程序。

 3) 要知道，对于调试 /tmp/demo/ 路径下的 main.exe 文件，将其作为 GDB 调试器的工作目录，一定程度上可以提高我们的调试效率。反之，如果 GDB 调试器的工作目录和目标调试文件不在同一目录，则很多时候需要额外指明要操作文件的存储路径（例如第 1) 种情况中用 file 指令指明调试文件时就必须指明其存储位置）。 

默认情况下，GDB 调试器的工作目录为启动时所使用的目录。例如在 ~ 路径下启动的 GDB 调试器，其工作目录就为 ~（当前用户的 home 目录）。当然，GDB 调试器提供有修改工作目录的指令，即 cd 指令。例如，将 GDB 调试器的工作目录修改为 /tmp/demo，则执行指令为：

```
(gdb) cd /tmp/demo
```

由此，GDB 调试器的工作目录就变成了 /tmp/demo。

4) 某些场景中，目标调试程序的执行还需要临时修改 PATH 环境变量，此时就可以借助 path 指令，例如：

```
(gdb) path /temp/demo
Executable and object file path: /temp/demo:/usr/local/sbin:/usr/local/bin...
```

> 注意，此修改方式只是临时的，退出 GDB 调试后会失效。

5) 默认情况下，GDB 调试的程序会接收 set args 等方式指定的参数，同时会将输出结果打印到屏幕上。而通过对输入、输出重定向，可以令调试程序接收指定文件或者终端提供的数据，也可以将执行结果输出到文件或者某个终端上。
例如，将 main.exe 文件的执行结果输出到 a.txt 文件中，执行如下命令：

```
(gdb) run > a.txt
```

由此，在 GDB 调试的工作目录下就会生成一个 a.txt 文件，其中存储的即为 main.exe 的执行结果。

```
[root@bogon demo]# pwd   <--显示当前工作路径
/tmp/demo
[root@bogon demo]# ls       <-- 显示当前路径下的文件
a.txt  main.c  main.exe
[root@bogon demo]# cd ~  <-- 进入 home 目录
[root@bogon ~]# gdb -q      <-- 开启 GDB 调试器
(gdb) cd /tmp/demo            <-- 修改 GDB 调试器的工作目录
Working directory /tmp/demo.
(gdb) file main.exe               <-- 指定要调试的目标文件
Reading symbols from main.exe...
(gdb) set args a.txt               <-- 指定传递的数据
(gdb) run                               <-- 运行程序
Starting program: /tmp/demo/main.exe a.txt
file open true[Inferior 1 (process 43065) exited normally]
```

# gdb设置断点

 在 GDB 调试器中对 C、C++ 程序打断点，最常用的就是 break 命令，有些场景中还会用到 tbreak 或者 rbreak 命令，本节将对这 3 个命令的功能和用法做详细的讲解。

为了让大家更好地了解给程序打断点的作用，这里以一段完整的 C 语言程序为例： 

```C
#include<stdio.h>
int main(int argc,char* argv[])
{
    int num = 1;
    while(num<100)
    {
        num *= 2;
    }
    printf("num=%d",num);
    return 0;
}
```

程序存储在/tmp/demo/main.c文件中，并已经生成了具备调试信息的 main.exe 可执行文件：

```
[root@bogon demo]# ls
main.c  main.exe
[root@bogon demo]# gdb main.exe -q
Reading symbols from main.exe...
(gdb)
```

## GDB break命令

break 命令（可以用 b 代替）常用的语法格式有以下 2 种。

```
1、(gdb) break location     // b location
2、(gdb) break ... if cond   // b .. if cond
```

 1) 第一种格式中，location 用于指定打断点的具体位置，其表示方式有多种，如表 1 所示。 

| location 的值     | 含 义                                                        |
| ----------------- | ------------------------------------------------------------ |
| linenum           | linenum 是一个整数，表示要打断点处代码的行号。要知道，程序中各行代码都有对应的行号，可通过执行 l（小写的 L）命令看到。 |
| filename:linenum  | filename 表示源程序文件名；linenum 为整数，表示具体行数。整体的意思是在指令文件 filename 中的第 linenum 行打断点。 |
| + offset - offset | offset 为整数（假设值为 2），+offset 表示以当前程序暂停位置（例如第 4 行）为准，向后数 offset 行处（第 6 行）打断点；-offset 表示以当前程序暂停位置为准，向前数 offset 行处（第 2 行）打断点。 |
| function          | function 表示程序中包含的函数的函数名，即 break 命令会在该函数内部的开头位置打断点，程序会执行到该函数第一行代码处暂停。 |
| filename:function | filename 表示远程文件名；function 表示程序中函数的函数名。整体的意思是在指定文件 filename 中 function 函数的开头位置打断点。 |

 2) 第二种格式中，... 可以是表 1 中所有参数的值，用于指定打断点的具体位置；cond 为某个表达式。整体的含义为：每次程序执行到 ... 位置时都计算 cond 的值，如果为 True，则程序在该位置暂停；反之，程序继续执行。

如下演示了以上 2 种打断点方式的具体用法： 

```
(gdb) l
1 #include<stdio.h>
2 int main(int argc,char* argv[])
3 {
4     int num = 1;
5     while(num<100)
6     {
7         num *= 2;
8     }
9     printf("num=%d",num);
10   return 0;
(gdb)
11 }
(gdb) b 4          <-- 程序第 4 行打断点
Breakpoint 1 at 0x1138: file main.c, line 4.
(gdb) r              <-- 运行程序，至第 4 行暂停
Starting program: /home/ubuntu64/demo/main.exe

Breakpoint 1, main (argc=1, argv=0x7fffffffe078) at main.c:4
4     int num = 1;
(gdb) b +1        <-- 在第 4 行的基础上，在第 5 行代码处打断点
Breakpoint 2 at 0x55555555513f: file main.c, line 5.
(gdb) c             <-- 继续执行程序，至第 5 行暂停
Continuing.

Breakpoint 2, main (argc=1, argv=0x7fffffffe078) at main.c:5
5     while(num<100)
(gdb) b 7 if num>10     <-- 如果 num>10 在第 7 行打断点
Breakpoint 3 at 0x555555555141: file main.c, line 7.
(gdb) c               <-- 继续执行
Continuing.

Breakpoint 3, main (argc=1, argv=0x7fffffffe078) at main.c:7
7         num *= 2;       <-- 程序在第 7 行暂停
(gdb) p num      <-- p 命令查看 num 当前的值
$1 = 16             <-- num=16
```



# 有益参考

1. [GDB调试教程：1小时玩转Linux gdb命令](http://c.biancheng.net/gdb/)
2. [GDB break（b）：设置断点](http://c.biancheng.net/view/8189.html)
