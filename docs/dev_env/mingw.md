# MinGW

[MinGW](http://www.mingw-w64.org/doku.php) 的全称是：Minimalist GNU on Windows 。它实际上是将经典的开源 C语言 编译器 GCC 移植到了 Windows 平台下，并且包含了 Win32API 和 MSYS，因此可以将源代码编译生成 Windows 下的可执行程序，又能如同在 Linux 平台下时，使用一些 Windows 不具备的开发工具。

一句话来概括：MinGW就是 GCC 的 Windows 版本 。

## 安装MinGW

进入 [MinGW下载页面](http://www.mingw-w64.org/doku.php/download), 选择自己需要的版本。为了安装方便，选择使用  **[Win-Builds](http://win-builds.org/doku.php)**  进行一键式下载安装。

![image-20210511205225415](pics/image-20210511205225415.png)

打开 win-builds 的安装程序，cmd命令框中开始执行解压文件 

![image-20210511205630055](pics/image-20210511205630055.png)

在弹出的窗口中选择 x86_64 的系统和安装位置

![image-20210511205807391](pics/image-20210511205807391.png)

为了加快安装速度，国内用户可以选择使用速度更快的源。如果没有，使用默认的速度也还行。

![image-20210511205956690](pics/image-20210511205956690.png)

然后点击 process 开始安装

![image-20210511210515377](pics/image-20210511210515377.png)

安装好，在安装目录的bin目录下可以看到g++/gcc的编译器

![image-20210511210705985](pics/image-20210511210705985.png)

## 配置环境变量

鼠标右键“我的电脑”->“属性”，选择“高级”选项卡下的“环境变量”，在系统变量里点“新建”，之后填写MinGW的安装路径。

 

![img](pics/145902-501a3cd5212f3c82.png)

在Path变量最前面添加下面这段声明后点击确定

```
%MinGW%\bin;
```

打开命令行，输入gcc -v查看编译器版本。

![image-20210511211132411](pics/image-20210511211132411.png)

## 测试编译

测试文件test.c，代码如下

```C
#include <stdio.h>

void main() {
        printf("Hello World!");
}
```

编译命令, 输出结果为：test.exe

```shell
gcc test.c -o test
```

执行 `test.exe`结果

 ![image-20210511211239430](pics/image-20210511211239430.png)