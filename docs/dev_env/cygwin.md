# Cygwin

## 简介

cygwin是一个在windows平台上运行的unix模拟环境，是cygnus solutions公司开发的自由软件。

cygwin是一个在windows平台上运行的 linux模拟环境,使用一个Dll(动态链接库)来实现。这样，我们可以开发出Cygwin下的UNIX工具,使用这个DLL运行在Windows下。

## 安装

Cygwin不是一键就能安装，它只是一个下载器。

1、双击安装包

 ![在这里插入图片描述](pics/20190603165139333.png) 


2、三种安装模式

①Install from Internet，这种模式直接从Internet安装，适合网速较快的情况；

②Download Without Installing，这种模式只从网上下载Cygwin的组件包，但不安装；

③Install from Local Directory，这种模式与上面第二种模式对应，当你的Cygwin组件包已经下载到本地，则可以使用此模式从本地安装Cygwin

我们选择第一种

 ![在这里插入图片描述](pics/20190603170707887.png) 

3、选择安装路径

 ![在这里插入图片描述](pics/20190603171121501.png) 

4、在下载的同时，Cygwin组件也保存到了本地，以便以后能够再次安装，这一步选择安装过程中从网上下载的Cygwin组件包的保存位置

 ![在这里插入图片描述](pics/20190603171700298.png) 

5、这一步选择连接的方式，选择你的连接方式，然后点击下一步，会出现选择下载站点的对话框，如下图所示

 ![在这里插入图片描述](pics/20190603172645520.png) 

①Use System Proxy Settings 使用系统的代理设置

②Direct Connection 一般多数用户都是这种直接连接的网络，所以都是直接使用默认设置即可

③Use HTTP/FTP Proxy 使用HTTP或FTP类型的代理。如果有需要，自己选择此项后，设置对应的代理地址和端口

6、选择下载站点
不同的镜像存放了不同的包，为了获得最快的下载速度，我们可以添加网易开源镜像http://mirrors.163.com/pics/ 或者 阿里云镜像http://mirrors.aliyun.com/pics/

 ![在这里插入图片描述](pics/20190603174248944.png) 

 ![在这里插入图片描述](pics/20190603174327467.png) 

7、开始加载

![在这里插入图片描述](pics/20190603174709456.png) 

8、选择需要下载安装的组件包

此处，对于安装Cygwin来说，就是安装各种各样的模块而已。最核心的，记住一定要安装Devel这个部分的模块，其中包含了各种开发所用到的工具或模块

展开devel

![在这里插入图片描述](pics/20190603180453390.png) 

从中选择gcc-core、gcc-g++、make、gdb、binutils、cmake、mingw进行安装，找到以下选项，点击后边的skip，使其变为版本号即可。（版本不要使用太高，8.3.0即可）

![在这里插入图片描述](pics/20190603180622772.png) 

9、确认改变，进行安装

![在这里插入图片描述](pics/20190603181603559.png) ![在这里插入图片描述](pics/20190603181715698.png)

> 这一步在安装 `*.bash` 之类的文件时，如果机器性能一般，可能会等待较长时间。耐心等待即可。

10、安装完成验证Cygwin

 运行cygwin

![在这里插入图片描述](pics/20190604084153833.png) 

在弹出的命令窗口输入

```
cygcheck -c cygwin
```

会打印出当前cygwin的版本和运行状态，如果status是ok的话，则cygwin运行正常

查看gcc和g++的版本

会打印出当前cygwin的版本和运行状态，如果status是ok的话，则cygwin运行正常

```
gcc --version
g++ --version
```

## 有益参考

1. [Cygwin安装教程](https://blog.csdn.net/u010356768/article/details/90756742)