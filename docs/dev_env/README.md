# 开发环境

* cygwin
* mingw
* clion
* vscode


## 有益参考

博客

1. [C++环境设置](https://www.runoob.com/cplusplus/cpp-environment-setup.html)
2. [VSCode配置C/C++环境](https://zhuanlan.zhihu.com/p/87864677)

视频

1. [在VSCode搭建C/C++环境【秒杀Visual C++/Dev C++](https://www.bilibili.com/video/BV1nt4y1r7Ez)

