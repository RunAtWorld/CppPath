# CLion
## 使用clion开发C++

[CLion](https://www.jetbrains.com/zh-cn/clion/)是一款专为开发C及C++所设计的跨平台IDE。它是以IntelliJ为基础设计的。

包含的功能特性：

- 提供C及C++支持(包含C++11, libc++ 和Boost)，同时也支持JavaScript, XML, HTML 和CSS 。
- 跨平台：您可在64-bit Linux，OS X 以及64-bit Windows上使用它。
- 支持GCC、clang、MinGW、Cygwin编译器以及 GDB 调试器。
- 提供对CMake 支持：包含自动处理CMake changes和CMake targets，更新新创建的 C/C++ 档案及CMake Cache 编辑器。
- 提供各式编码辅助：包含多行编辑功能、智能完成功能以及一键导航等。
- 安全可信的自动代码重构功能。
- 代码分析功能：监控代码品质并提供快速修复让开发人员得以及时就地解决问题。
- 集成GDB 调试器及评估表达式(expressions)功能、STL 容器渲染器(renderers)、监视(watches)、内嵌变量视图等。
- 版本控制系统集成：Subversion、Git、GitHub,、Mercurial、CVS、Perforce及TFS。
- 内建terminal 模式并可通过插件实现提供Vim-emulation模式。
- 智能编辑器-CLion的智能编辑器能帮助开发人员更快地将代码补全，同时保持开发人员的代码格式化并符合配置编码的风格。

支持以下GCC/G++的编译器。

![image-20210511203943490](pics/image-20210511203943490.png)

使用前，使用前可以先装好 MinGw,Cygwin,WSL,Visual Studio中的一种或几种。Remote Host 为使用远程GCC/G++。

## 使用Clion编译C++

点击左上角的**File**，下拉列表选择点击**Settings**, 根据自己的情况选好以下工具所在的目录。如果安装，参考[MinGw安装](docs/config_env/mingw.md) 和 [Cygwin](docs/config_env/cygwin.md)安装。

![image-20210511204344706](pics/image-20210511204344706.png)

配置 CMake 使用的编译器

![image-20210511211731618](pics/image-20210511211731618.png)