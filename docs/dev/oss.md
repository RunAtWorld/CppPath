# OSS

## 一、OSS C++ SDK下载

官方提供了主流语言的 SDK 源码以及示例代码在[SDK示例简介](https://help.aliyun.com/document_detail/52834.html?spm=a2c4g.11186623.6.931.347df2eeQ0vijP)，但我们只介绍 C++ 版本的。

OSS C++ SDK 源码的下载链接为：[aliyun-oss-cpp-sdk](https://github.com/aliyun/aliyun-oss-cpp-sdk?spm=a2c6h.13321295.0.0.68fc5c2dDYg4r0)

OSS C++ SDK 也提供了丰富的示例代码，方便参考或直接使用：[SDK示例代码](https://help.aliyun.com/document_detail/103185.html?spm=a2c4g.11186623.6.1464.768026fdRfmovk)

## 二、CMake编译

下载后的`aliyun-oss-cpp-sdk`要使用 CMake 编译，这个 SDK 没有任何库依赖，CMake 很简单，就是使用 cmake-gui 添加 CMakeLists 所在目录和下一级的 build 目录，点击 configure 和 generate 按钮即可生成 .sln 工程.

![OSS_CMakeSdk_A.png](https://i.loli.net/2021/03/07/oTyrcjn6A9ClZDK.png)

点击configure注意选择对应VS版本和平台

![image-20220304163557139](pics\image-20220304163557139.png)

如果不用cmake-gui，也可以用cmake命令行

以VS2013为例，在build目录下输入命令

```
cmake -A x64 ..  -G "Visual Studio 12 2013"
```

`cmake -g` 可以查看支持的VS版本

再使用 VS 编译即可生成所需要的lib库。编译也可以参考[OSS C++ SDK安装](https://help.aliyun.com/document_detail/106216.html)

![image-20220304164555030](pics\image-20220304164555030.png)

## 三、创建工程、列举文件示例

新建一个项目 ossExample，切换为 64 位，拷贝添加`aliyun-oss-cpp-sdk-master\sdk\include\alibabacloud\oss`目录下的头文件到工程新建的 include 目录下， 然后添加`aliyun-oss-cpp-sdk-master\build\lib\Debug`和`aliyun-oss-cpp-sdk-master\third_party\lib\x64`目录下的库文件到新建的 lib 目录下。`aliyun-oss-cpp-sdk-master\third_party\lib\x64`目录下的DLL文件拷贝到最终生成的exe目录下。

在附加包含目录处添加拷贝后的头文件所在路径，在附加库目录处添加 .lib 文件所在路径，在附加依赖项处添加 .lib 文件（alibabacloud-oss-cpp-sdk.lib、libcurl.lib、libeay32.lib、ssleay32.lib），配置完成之后添加 mian.cpp。

详细图文步骤可以参考：[使用阿里云 OSS C++ SDK](https://blog.csdn.net/liang19890820/article/details/105071482/)

**列举文件**

存储空间是 OSS 全局命名空间，相当于数据的容器，可以存储若干文件。 以下代码用于新建一个存储空间（main.cpp）：

```c++
#include <alibabacloud/oss/OssClient.h>
using namespace AlibabaCloud::OSS;

int main(void)
{
    /* 初始化OSS账号信息 */
    std::string AccessKeyId = "yourAccessKeyId";
    std::string AccessKeySecret = "yourAccessKeySecret";
    std::string Endpoint = "yourEndpoint";
    std::string BucketName = "yourBucketName";

    /* 初始化网络等资源 */
    InitializeSdk();

    ClientConfiguration conf;
    OssClient client(Endpoint, AccessKeyId, AccessKeySecret, conf);

    /* 列举文件 */
    ListObjectsRequest request(BucketName);
    auto outcome = client.ListObjects(request);

    if (!outcome.isSuccess()) {    
        /* 异常处理 */
        std::cout << "ListObjects fail" <<
        ",code:" << outcome.error().Code() <<
        ",message:" << outcome.error().Message() <<
        ",requestId:" << outcome.error().RequestId() << std::endl;
        ShutdownSdk();
        return -1;  
    }
    else {
        for (const auto& object : outcome.result().ObjectSummarys()) {
            std::cout << "object"<<
            ",name:" << object.Key() <<
            ",size:" << object.Size() <<
            ",last modified time:" << object.LastModified() << std::endl;
        }      
    }

    /* 释放网络等资源 */
    ShutdownSdk();
    return 0;
}
```

OSS 账号信息改写成你本人的信息。第一次运行会报错缺少动态库，将动态库（libcurl.dll、libeay32.dll、ssleay32.dll、zlibwapi.dll）考到输出 debug 目录下，正常运行。

上面只列举了一个 OSS C++ SDK 的示例-列举文件，更多示例请参考：

- [SDK-Git示例代码](https://help.aliyun.com/document_detail/103185.htm?spm=a2c4g.11186623.2.9.5a36f2eeFtdBZ6#concept-hcq-cgl-ngb)
- [SDK示例-快速入门](https://help.aliyun.com/document_detail/106556.html?spm=a2c4g.11186623.6.1467.f434533875ioTc)
- [SDK示例-各种上传方式](https://help.aliyun.com/document_detail/106621.html?spm=a2c4g.11186623.6.1487.5fac207dUD4OBR)
- [SDK示例-各种下载方式](https://help.aliyun.com/document_detail/106625.html?spm=a2c4g.11186623.6.1496.171e7a74v3y6gE)
- [SDK示例-各种文件管理接口](https://help.aliyun.com/document_detail/106626.html?spm=a2c4g.11186623.6.1503.12557a74iwzHFz)
- [SDK示例-管理版本控制](https://help.aliyun.com/document_detail/154956.html?spm=a2c4g.11186623.6.1515.38777a74hnNDJk)
- [SDK示例-对象标签](https://help.aliyun.com/document_detail/122359.html?spm=a2c4g.11186623.3.5.61e048f2m9eYTk)
- [SDK示例-数据加密](https://help.aliyun.com/document_detail/163616.html?spm=a2c4g.11186623.3.5.6f87534chMkt9i)

## 四、封装OSS操作类

下面将实现对 OSS 的常见操作函数并封装成类 - OssOperate。

OssOperate.h

```c++
#pragma once
#include <alibabacloud/oss/OssClient.h>
using namespace AlibabaCloud::OSS;

// OSS操作类
class OssOperate
{
public:
	// 单例模式（最推荐的懒汉式，线程安全）
	static OssOperate &GetInstance() {
		static OssOperate m_instance;
		return m_instance;
	}
	// 创建存储空间
	bool createBucket();
	// 上传文件
	bool upload(std::string objectName, std::string localName);
	// 下载文件
	bool download(std::string objectName, std::string nameToSave);
	// 删除文件
	bool delFile(std::string objectName);
	// 列举文件
	bool listObjects();

private:
	// 构造函数定义为 private，这就保证了必须通过GetInstance方法才能获取到唯一的m_instance实例
	OssOperate();

	// OSS客户端
	OssClient *m_client;
	// OSS账号信息
	std::string m_accessKeyId;
	std::string m_accessKeySecret;
	std::string m_endpoint;
	std::string m_bucketName;
};
```

OssOperate.cpp

```c++
#include "OssOperate.h"

OssOperate::OssOperate()
{
    // 初始化OSS账号信息
    m_accessKeyId = "yourAccessKeyId";
    m_accessKeySecret = "yourAccessKeySecret";
    m_endpoint = "yourEndpoint";
    m_bucketName = "yourBucketName";

    // yourObjectName表示上传文件到OSS时需要指定包含文件后缀在内的完整路径，例如test/123.png
    std::string ObjectName = "test/test555.png";

    // 初始化OSS客户端
    ClientConfiguration conf;
    m_client = new OssClient(m_endpoint, m_accessKeyId, m_accessKeySecret, conf);

    // 初始化网络等资源
    InitializeSdk();
}

// 创建存储空间
bool OssOperate::createBucket()
{
    // 指定新创建bucket的名称、存储类型和ACL
    CreateBucketRequest request(m_bucketName, StorageClass::IA, CannedAccessControlList::PublicReadWrite);

    // 创建bucket
    auto outcome = m_client->CreateBucket(request);

    if (!outcome.isSuccess()) {
        // 异常处理
        std::cout << "CreateBucket fail" <<
            ",code:" << outcome.error().Code() <<
            ",message:" << outcome.error().Message() <<
            ",requestId:" << outcome.error().RequestId() << std::endl;
        ShutdownSdk();
        return false;
    }

    // 释放网络等资源
    ShutdownSdk();

    return true;
}

// 上传文件
bool OssOperate::upload(std::string objectName, std::string localName)
{
    // yourLocalFilename由本地文件路径加文件名包括后缀组成，例如C:\\Users\\feng\\Desktop\\123.png
    auto outcome = m_client->PutObject(m_bucketName, objectName, localName);
    // 异常处理
    if (!outcome.isSuccess()) {        
        std::cout << "PutObject fail" <<
            ",code:" << outcome.error().Code() <<
            ",message:" << outcome.error().Message() <<
            ",requestId:" << outcome.error().RequestId() << std::endl;
        ShutdownSdk();
        return false;
    }

    // 释放网络等资源
    ShutdownSdk();

    return true;
}

// 下载文件
bool OssOperate::download(std::string objectName, std::string nameToSave)
{
    // 获取文件到本地文件
    DownloadObjectRequest request(m_bucketName, objectName, nameToSave);
    auto outcome = m_client->ResumableDownloadObject(request); // 官方的下载代码报错，这里改写了

    if (outcome.isSuccess()) {
        std::cout << "GetObjectToFile success, ContentLength is" << outcome.result().Metadata().ContentLength() << std::endl;
    }
    else {
        // 异常处理
        std::cout << "GetObjectToFile fail" <<
            ",code:" << outcome.error().Code() <<
            ",message:" << outcome.error().Message() <<
            ",requestId:" << outcome.error().RequestId() << std::endl;
        ShutdownSdk();
        return false;
    }

    // 释放网络等资源
    ShutdownSdk();

    return true;
}

// 删除文件
bool OssOperate::delFile(std::string objectName)
{
    // 删除操作
    DeleteObjectRequest request(m_bucketName, objectName);
    auto outcome = m_client->DeleteObject(request);

    if (!outcome.isSuccess()) {
        // 异常处理
        std::cout << "DeleteObject fail" <<
            ",code:" << outcome.error().Code() <<
            ",message:" << outcome.error().Message() <<
            ",requestId:" << outcome.error().RequestId() << std::endl;
        ShutdownSdk();
        return -1;
    }

    // 释放网络等资源
    ShutdownSdk();

    return true;
}

// 列举文件
bool OssOperate::listObjects()
{
    ListObjectsRequest request(m_bucketName);
    auto outcome = m_client->ListObjects(request);

    if (!outcome.isSuccess()) {
        // 异常处理
        std::cout << "ListObjects fail" <<
            ",code:" << outcome.error().Code() <<
            ",message:" << outcome.error().Message() <<
            ",requestId:" << outcome.error().RequestId() << std::endl;
        ShutdownSdk();
        return false;
    }
    else {
        for (const auto& object : outcome.result().ObjectSummarys()) {
            std::cout << "object" <<
                ",name:" << object.Key() <<
                ",size:" << object.Size() <<
                ",last modified time:" << object.LastModified() << std::endl;
        }
    }

    // 释放网络等资源
    ShutdownSdk();

    return true;
}
```

main.cpp

```c++
#include "OssOperate.h"

int main(void)
{
    // 上传文件
    OssOperate::GetInstance().upload("test/123.png", "C:\\Users\\feng\\Desktop\\test555.png");
    // 下载文件
    OssOperate::GetInstance().download("test/123.png", "C:\\Users\\feng\\Desktop\\456.png");
    // 删除文件
    OssOperate::GetInstance().delFile("test/123.png");
    // 列举文件
    OssOperate::GetInstance().listObjects();

    return 0;
}
```

## 参考
1. [阿里云对象存储 OSS](https://help.aliyun.com/document_detail/106216.html)
1. [OSS C++ SDK使用总结](https://www.cnblogs.com/linuxAndMcu/p/14497266.html)
1. [使用阿里云 OSS C++ SDK](https://blog.csdn.net/liang19890820/article/details/105071482/)
1. [VS项目中引入dll的方法](https://blog.csdn.net/jacke121/article/details/54880601)
1. [windows 下使用cmake指定visual studio 版本](https://blog.csdn.net/iceboy314159/article/details/87829950)